# Stronghold Crusader AIVs by Heroesflorian

## How to use 

### What do AIVs do and where to find them?
In your Stronghold Crusader install directory (e.g. `Program Files (x86)/Firefly Studios/Stronghold Crusader`), there is a subfolder `aiv` which by default contains a number of `.aiv` (AI Village) files. Each of those files contains the plans for one castle of one AI character, named by the corresponding (English) name of the ingame character, followed by a digit from 1 to 8. When you start a skirmish game, Crusader picks one of the available castle plans for every character you placed on the map, except for skirmish trail missions where the number to be chosen for each character is hardcoded (e.g. in Mission 16, saladin6.aiv for the right Saladin on the map). 

### How do I change them?
By removing or replacing some (or all) of a character's castle plan files from your game's `aiv` folder, you can change and restrict what castles a character will build in the game. If you want to use a particular set of aivs, you can also replace your game's entire `aiv` folder instead. 

### Notes
- It is sufficient to rename a file instead of deleting it, and the `aiv` folder may contain an arbitrary number of renamed files, or subfolders - in case you'd like to store different sets of `.aiv` files right there - without having an effect on the game. 

- If there are no castle plans present for a character when you start Crusader, the character will usually not be available to add for a skirmish game. If you remove a character's aiv files while Crusader is open already, or remove the files, then open Crusader, load a savegame with the corresponding character and then try to restart that, or if no aiv file with the number hardcoded for a particular skirmish trail mission is there, a character will end up with no castle at all, making it more or less unable to function. 

- Multiplayer: If you want to use modified aiv files in multiplayer games, make sure that all players have the same aiv files for all characters you plan to include in a game, *before* starting Crusader. If the files differ, or if changes were made after starting Crusader, the same behavior may occur as for the above case (no castle plans present). 

- It is recommended to back up the regular aiv files before replacing them, so you are able to revert back to the default castles at a later point in time. If you forgot to do this, or are unable to find your backup copy, you can find all of the default castles in the `firefly_aiv` subfolder of this project. 








ToDo




















## Saladin and the Quest for Ultimate Power
Saladin commonly is seen as a rather strong AI character in Stronghold Crusader, perhaps even the most powerful one from the Arabian ones, but nonetheless is far from being perfect. Depending on the conditions, he does not only lose against the overally strongest character, the Wolf, but also struggles or loses against many others. And for most human opponents, Saladin's rather simple castle designs usually do not provide a major challenge either, unlike some of the castles from some other characters. All in all, Saladin would have a lot of additional potential when looking at what he has available on paper, and some of that can be brought out with a proper castle (and some would require Firefly to rework or make their character-specific castle-independent AI behavior moddable). 

### The Basics
Saladin is usually known mostly for his strong economy and a general tendency for his treasury to fill up pretty quickly if he is left alone for a while. Moreover, albeit to a slightly lesser extent, his quite decent (but not necessarily top-notch) siege attacks as well as his use of many (horse/infantry) archers for patrols make him one of the more powerful AI characters. But despite his fairly strong economy, the typical wealth of Saladin depends heavily on the behavior of himself as well as his opponents. 



### The Details - General Characteristics of Saladin (skip or prepare for a longer read)
Below I will try to explain behavior and characteristics of Saladin, and some general AI quirks, in more detail. 

##### Saladin's Economy in a Nutshell
When he got everything running, Saladin's economy is powerful, but getting it running and keeping it running is the hard part. Typically, Saladin occupies huge amounts of grassland for his numerous farms, in addition to running 4 quarries, 4 iron mines, 1 pitch rig and a large amount of woodcutters, some even as part of in his castle designs. If all of those can gather goods, Saladin can happily gather gold. But in order to function, all of that economy outside his castle walls needs protection, i.e. soldiers that defend it against attacks. Also, especially early in the game, Saladin relies a lot on gathering a lot of wood in order to set up all of his many, many buildings reasonably quickly. 

##### Saladin's Patrols
Therefore, Saladin amasses impressively large amounts of archers whenever he can, and sends them around in order to pick off anyone daring to harm his numerous workers. Moreover, he has some groups of archers, fire throwers and Arabian swordsmen casually patrolling all of his exterior economy from time to time, independent of hostile disruptions. (Note: As those do not play a big role usually, in the following, only Saladin's more actively used archer groups will be referred to as "patrols".) 
All of those archers sent out to protect Saladin workers - wherever they recently got harmed - live dangerous lives, however, as they constantly move near the frontlines - or sometimes even beyond them - and thus frequently are in range of hostile archers, ballistae, mangonels or other troops and defensive structures of Saladin's opponents. That implies Saladin is bound to lose a good number of archers all the time in order to keep all of his exterior economy safe. Moreover, Saladin uses fairly expensive troops in general. 

##### Complex Economic Dependences
This essentially means Saladin needs many troops to protect his exterior economy and a strong overall economy in order to obtain a lot of gold in order to recruit large amounts of expensive troops. Sort of a cycle - but not quite there yet: For the overall economy not only the exterior part is relevant, but also the interior economic buildings inside the castle. For Saladin's regular castles, this interior economy consists of beer production (and consumption / inns), bread production as well as iron equipment production. All of those production chains rely rather heavily on goods usually gathered from outside the castle, such as wheat, hops and iron. This makes Saladin even more dependent of his exterior economy, especially his farms, than the raw value of the gathered goods suggests: Cut off the supply chain by killing farm workers, destroying farms or closing Saladin's castle gates and also the refinement chains inside will suffer. 

In some more detail: 
While the iron equipment production will simply turn into a non-profit hobby that just occupies valuable space without any advantage for Saladin, whenever he cannot get iron delivered from his mines and has to purchase iron instead, Saladin does at least not rely on the output from this production, making it non-vital to keep running. 
For his beer and bread productions, though, Saladin needs them running for most of the time to survive in the long term. Thus, purchasing large quantities of hops and wheat in times of shortage is something Saladin will happily do at any point in time - if he has enough gold to do so. But he will prioritise recruiting troops over trading, so if Saladin is out of gold, he will not save it up for buying wheat. 
A bread production without active wheat farms is sustainable on its own, but given Saladin's large ongoing expenses for recruiting troops (be it patrols or the next siege attacking force), a bread production without a large-enough starting capital as buffer, or ongoing exterior income from quarries, farms and mines is not sufficient to keep Saladin from going bankrupt and breaking down eventually. Also, Saladin partially lives off selling bread, so when his bread production breaks down, his income will decrease further. 
Furthermore, Saladin (and any other relevant AI character) will not buy flour or beer (or iron) directly, unless they have run out of those goods completely (0 in stockpile and no new goods of that type incoming for about 1 ingame month), and even then he will only buy 2 units. To make matters worse, before purchasing those, the AI will also set asleep all production buildings that rely on that good. For Saladin this means, running out of flour will cause all bakeries to be turned asleep eventually, even if some of them are still currently baking bread, causing the half-baked bread to be lost completely. Also, due to the fact Saladin has a lot more bakeries than just 2, buying 2 flour will not solve that issue, as again most bakers will end up without flour, causing the setting-asleep-cycle to repeat... 

So essentially, for Saladin's economy to work, he requires several conditions to be fulfilled: 1. Flour production must (nearly) cover flour consumption at any time, 2. Beer production must cover beer consumption at any time, 3. To enable sufficient flour and beer production, either hops and wheat gathering capacity has to keep up with consumption rates for beer and flour, or Saladin needs to have enough spare money in the treasury to buy large quantities of hops and wheat (purchases are made in sizes of 8-10 units at a time). 

##### Saladin's Need for Peasants
Moreover, having a good amount of free peasants available for recruiting troops is helpful to reduce the risk of "jamming" his recruitment capabilities due to a lack of recruits. Having said that, high popularity and thus a high peasant respawning rate is probably more essential for Saladin than having many peasants at once, as it can easily happen that he recruits nearly constantly, making respawn rate the limiting factor to recruiting - but popularity management is something that can barely be influenced by the aiv given to an AI character. Either way, a lack of peasants - be it due to temporarily low popularity, a lack of houses or simply an increased death rate of his workers - impacts Saladin much more than many other AI characters due to the reasons mentioned above. 


##### Saladin's Main Melee Unit Sucks...
Now, that's his economic aspects covered pretty much. What about his military? 
Well... Saladin uses a rather wide variety of Arabian troops which is decent. But his main melee unit, the Arabian swordsman, is simply underpowered: Slow, at best mediocre armour, horribly low attack speed against walls and buildings, cannot dig moats, and clearly inferior in health and damage as compared to European swordsmen. Possibly they could be described roughly as the result of mixing macemen with European swordsmen by taking the worse of both values for every stat: Fragile and mid-powered like macemen, but moat-powerless and nearly as slow as European swordsmen, with a fairly high price point and worse attack rates against structures. When he can amass enough of his Arabian swordsmen thanks to a strong economy, Saladin could possibly get away with that for melee situations. But then again, Saladin generally uses far less of them for his siege attacks than many (except Nizar, all?) other Arabian characters do. Ouch... As a consequence, Saladin lacks a lot in the brute-force melee department, and thus has to rely mostly on ranged warfare and economic power to beat opponents. (The exception would be very big late-game attacks, if Saladin keeps recruiting additional troops (mostly swordsmen) nonstop when can for whatever reason not reach his desired attack target or a human ally asks for protection. But generally, that's not the likely / frequent / average case.) 

##### ...And Why This Is So Bad Overall
Okay, so what about the ranged warfare? Well, Saladin completely lacks heavily armoured troops, especially when facing defenses with crossbowmen. Those snipe away archers and Arabian swordsmen alike with just a few shots, and take a lot of hits (comparably) from Saladin's archers. So, yes, Saladin can do ranged warfare, but against strongly manned defenses, he will have dramatic losses for relatively little gains. To keep that up, a strong economy and a good defense are required. And even then, Saladin sometimes simply runs into limits in terms of recruitment speed. 
Also, with Saladin being unable to do anything in terms of melee combat against crossbow-wielders, ranged warfare is Saladin's only option to beat certain AI characters at all. With the horrible win-loss ratio of pure ranged warfare against fortified opponents, that makes it hard for Saladin to beat those without stretching their finances beyond the limits of their economy. 
And yes, Saladin uses fire ballistae and catapults for his sieges, but no trebuchets, and no overwhelmingly large amounts of catapults, and without armored melee troops his siege engines and archers only have a limited amount of time for shooting and dealing damage, before the siege attack is effectively over. 

##### Saladin's Economic Warfare
So... err... economic warfare, right? Sure... for that he uses archers. Great to kill hostile workers and decimate hostile ranged patrols and unarmored melee troops from a distance. Less great when the enemy sends armoured melee troops against Saladin's archers, as they do not pierce armour particularly well (they're no overpowered crossbowmen after all) and quickly get slaughtered in melee combat against specialized melee troops. But most importantly... Saladin again lacks in the "brute force" department: He rarely ever sends melee troops to attack hostile economy outside of big siege attacks, and even when he does... well, Arabian swordsmen are expensive, suck against buildings, are slow and not particularly sturdy - in other words, possibly the worst choice for that task. 
But wait, Saladin also uses slaves! Yes, but again, only during sieges. But wait, Saladin also uses assassins! Yes, but once again, only during sieges. Meh. So hostile farms and quarries and other buildings are mostly safe and stay unharmed. Dammit! Okay then, but Saladin uses siege engines - fire ballistae and catapults, hah! Yes, but again... fire ballistae only during sieges. And catapults only reliably during sieges. Sometimes Saladin sends disruption catapults outside siege attacks, but neither reliably nor constantly, nor even at all, in the long term at least. 
Thus, essentially, Saladin's economic warfare comes down mostly to archers. What he can do with those is intercept resource deliveries, close hostile castle gates to lock out resource deliveries if Saladin is lucky, and most importantly force the opponent to constantly send own troops against Saladin's archers. Many can not sustain that counter-pressure economically, which is one thing Saladin is fairly good at. 

So, all in all, Saladin is not the greatest attacker or harrasser, but okayish. 


##### Saladin's Defenses...
For Saladin's defenses... he does use moat, but not a lot of it for his standard castles. He can use pitch ditches, but his standard castles don't. Same for oil pots, dog cages, killing pits. Defensive trebuchets or fire ballistae? Nope, not with the standard castles. Mhmm... maybe crossbowmen, to deal with heavily armoured melee troops? Nope. So what does this Saladin dude use at all then? First of all, he has both types of tower-mounted siege engines (ballistae, mangonels). Secondly, archers. And thirdly, fire throwers - although those are often times not used in sufficient numbers. All in all, Saladin lacks efficient means to deal with larger bulks of heavily armored melee troops. He mostly needs to rely on them walking into the firing range of what few fire throwers Saladin has on his walls and keep, and on outspamming his opponents with more troops when they breach into his castle. 

##### ...And Lack Thereof
Considering the AI generally are not that smart about saving up their troops for defense, that leaves Saladin fairly vulnerable against European swordsmen, pikemen and, to a lesser extent, Arabian swordsmen and macemen. And yes, Saladin likes to spam archers, which is helpful to defend himself. Sadly, though, Saladin places less archers on his towers than on the ground and while he mans his towers decently, they're no match to the defenses of the Wolf or a late-game Frederick in terms of total ranged troops on towers and only on par with most other Arabian characters. 
What  Saladin's defense certainly has are decent castle layouts, multiple gates, relatively thick walls and, as already mentioned, tower-mounted siege engines, so it is solid overall. Yet, no multiple layers of defenses, wall-protected towers, large moats or traps. Stuff  that makes e.g. a Wolf, Nizar or even a Caliph challenging or at least annoying and time consuming to beat even when they cannot spam troops and do not have endless gold reserves. 

##### FIRE
One more thing to note about Saladin: He has lots of buildings inside and outside his castle walls, making him very vulnerable to fire. And you can say what you want about fire in the Stronghold series being a bit overly powerful, but certainly Saladin is considerably more prone to lose a game due to catching fire than many other AI characters are. One cannot change the "strategy"/behavior of well workers, nor the fire spreading mechanics of the game, nor the fact that AI will set wells and water pots asleep when there is no fire to extinguish and no gold reserves in the treasury to "afford the luxury" of having wells manned permanently. But one can certainly try to keep fire in mind when designing a castle. 


 
### The Origins
Back then sometime in late 2009 / early 2010, I had come up with the - probably not that uncommon - idea to modify Saladin's castles in order to make him a bit tougher to beat in defense and stronger in offense. 
However, unlike many others, I thought a negative fear factor instead of vanilla Saladin's positive fear factor would be more beneficial and went with that. I also did not care particularly much about trying to mimic his original castle layouts as closely as possible, or making the smallest possible castle to fit every spot and position on every skirmish trail mission or regular maps. Instead, I tried to use the means I had (i.e. the AIV Editor released by Firefly Studios as well as my personal experience and observations from the game, its AI characters and their regular castles) to try and create the best castle for Saladin that I could come up with. 

After some from-scratch designing, experimenting, testing and iterating, I eventually settled with a rather uncommon, slightly wobbly-looking castle layout filled to the brim with economic buildings and whatever else is necessary for a functioning AI castle. 

My internal "code name" for this castle was "Eco Saladin" due to the focus on improving his economy. 

##### Economic Makeover
The idea was cramming as much economic power and potential to the interior of the castle as possible / feasible, and then try to protect that as well as possible. Thus, Saladin would be more independent of exterior resource gathering and more resilient against economic disruptions by hostile troops. Also, with Saladin having beer production as well as 3 different types of food for popularity, as well as a large amount of production chains, a large castle with potentially long ways for workers to walk, I decided to swap out statues and gardens for gallows and gibbets, tripling his economic efficiency, in theory (\*). 
As Saladin relies on a strong economy to do what is most important for his military success - spam archers nonstop! - anyway, the reduced troop attack power should be compensated for by larger numbers. Plus, workers still get one-shot killed by Saladin's archers, and Saladin's central weakness, the not-properly-armored, slow, weak Arabian swordsmen, do often enough not get into melee action before dying either way, and troop defense/health is constant, independent of fear factor. 
(\* Of course, in exchange for lower tax rates. But, in relation to the malus on popularity, lower taxes are far more efficiency anyway. Also, while fear factor 0 to -5 provides an efficiency increase from 100% to 150% by means of a 50% higher product output per input resource used, for neutral to +5 fear factor the declared efficiency decrease to 50% is not entirely accurate, as it mostly decreases efficiency per time, not per input resource in many cases...)
To top things off, I gave Saladin religious buildings for a constant +3 religion bonus along with a (varying) +2 further religion bonus from 25%+ blessed people, to make sure Saladin can still put some taxes despite negatve fear factor. Also, to reduce dependence on iron mines and improve profits, I replaced Saladin's blacksmiths and armorers by fletchers who produce crossbows for Saladin now. Those are always paying off, even when buying the wood, and generally provide higher profit margins. They also do not suffer from the AI's unwillingness to properly plan resource purchases ahead of time, as Saladin wants to have some wood in his stockpile at any time either way (plus, wood buys much cheaper, making it more affordable even when low on gold to then get back more gold from crossbow sales). And lastly, as crossbows seem to be the main threat to (and lack of) Saladin, this way he can finally profit from them in some way at least. 

##### Economic Defenses
In order to not unnecessarily hinder economic work chains, a single-layer castle with several gates makes more sense than intricate multi-layered fortresses due to the gate-closing mechanics in SHC and how workers react to closed gates: When a few hostile troops outside the castle are enough to essentially paralyze a castle's whole economy due to paths getting blocked-off by closed gates, then the best defense is worthless as it will fall to starvation and desperated, self-demolishing measures of the AI trying to rebuild their broken economy. 

To compensate Saladin's lack of defensive fire-power (literally and figuratively), a moat all around, as well as defensive fire ballistae, pitch ditches, dog cages, killing pits and oil pots were added. 
Moreover, learning from Wazir, Emperor Frederick and even the Sultan, the Walls were shaped in a way that would reliably prevent assassins (and human players' siege towers) from passing the new Saladin's castle's walls' crenellations, without making it look too obvious or bulky (unlike that sentence's heap of genitive clauses ;D ). 

##### United AIV
Eventually, I was so satisfied with the castle I had created for Saladin that I even re-used the same castle (layout) and changed the castle's interior, troop spots and economic balancing to create 16 different versions of it, one for each of the 16 AI characters available in SHC. For many of them, this experiment worked quite well, making them tougher than with their standard castles after a bit of fine-tuning to account for character-specific peculiarities, so I decided to release that whole set of 16 same-layout-castles as a pack. This pack, for obvious reasons called "United AIV", can be found here: http://stronghold.heavengames.com/downloads/showfile.php?fileid=6427 or inside this repository in the subfolder "united aiv". 

From the pack's description: 
*Giving a castle with some high towers, golden-roofed buildings and a more profitable weapons production, a few fire ballistae and several "traps", surrounded by an original Crusade-proof moat to the mighty, clever Arabian Sultan Salah ad-Din may be wise if you got him as an ally, as he will thank you by holding off any enemy.
If you are his foe, however, you may better leave the desert - unless you want to end up as a dessert for hyenas.* 

##### FIRE 2.0
Furthermore - and this I sort of regretted later - I had decided to put all those gibbets and gallows all around the outside of the castle's walls. It looked kinda badass, and decorative, and it made obvious: This Saladin is not to be messed with! Also, if an attacker was shooting at the walls, he had good chances to destroy a few gibbets before even touching the walls, which would certainly make a re-building of the gibbets necessary later on, but also keep the walls intact for much longer (and also cause melee troops to partially not attack properly, due to inaccessible wall sections, thanks to stupid AI routines...). 
And without having to ever worry about gold anymore, replacing gibbets (cost some gold) later was seen by me to have less impact than having a breached wall with enemies pouring in right now, even if the cost for a gibbet (50 gold) outweighed the cost for 2-3 tiles of wall (1-2 stones, costing considerably less). And especially with the AI sending melee troops to attack walls, and walls only, or else just do nothing, it seemed like an even better trade-off (a bit cheaty though, agreed). 
Moreover, I had wrapped around a moat all around the gibbet-decorated walls, so on most maps, the gallows themselves should be fairly well-secured as well. Or so I thought. 

While they indeed cannot be lit on fire by slaves if and when the moat is intact, they can still be lit on fire by fire ballistae from afar... it certainly is iconic, yes, but also a weakness due to that, as the unreachable gallows behind the moat are also unreachable for any of Saladin's well workers, and fire can spread from gallow to gallow all around Saladin's castle, and also through the castle walls to the interior from the castle - and back out again, to another gibbet or gallow on the opposite side, unreachable for well workers. Together with the densely packed castle interior, this makes the "Eco Saladin" or "United AIV Saladin" castle even more vulnerable to fire than the regular castles, once it starts burning. Despite having several times more wells and water pots... 



### The Rework
The *Eco* version of Saladin's castle does well overall, but it still has a number of issues. This includes, most prominently, its weakness against fire due to a design flaw of mine, but also some other areas for improvement. Thus, I decided to revisit the old design and rework it, starting in late 2017. 

##### Goals for the Rework Process 
1. make the design more resistant to fire
2. make sure the design *really* is considerably more resistant to fire!!
3. decrease vulnerability against mangonels, especially regarding stair and wall access to vital towers. 
4. improve (or rather "support better") the AI behavior for creating siege-independent "disruption siege engines", i.e. for Saladin catapults. (although this is generally bugged for all AIs to some extent)
5. optimize economic balancing for a smoother-running economy that is more robust, efficient and even more profitable
6. improve and compactify build order where possible
7. improve handling of low starting goods (important for a large and expensive castle to get up and running in time)
8. rework pitch ditches (were beautifully curvy, but inefficient before, especially too close together)
9. increase the castle's "mid-range offensive capabilities" by adding trebuchets
10. try to beat the (vanilla) Wolf, even with his wolf6.aiv castle, in 1 vs 1 games! 

##### Name Change
As part of the rework, I also chose a new name for this Saladin castle, "**Black Saladin**". Not only is this good for distinguishing between the original and the reworked castle versions, but it also fits better in my opinion: This Saladin with negative fear factor and a castle featuring tons of gallows, gibbets, dungeons and stocks, as well as a bunch of added traps such as pitch-black pitch ditches, killing pits and war dogs does indeed feel darker, even black, compared to vanilla Saladin's cheerful gardens and trapless castles. 
The name **Black Saladin** also conveys nicely the dark threat this Saladin castle poses to anyone nearby due to the defensive siege engines and improved economy that lets Saladin spam troops even more than usually. Last but not least, already the "Eco Saladin" did not just want a stronger economy for the sake of itself, but to ultimately make Saladin more powerful and dangerous, so "Eco" might have been a bit misleading anyway. 

##### Rework Steps
0. Eco Saladin (base)
1. Eco Saladin v2.0, a.k.a. Black Saladin: 
2. 



-------------
*******************



