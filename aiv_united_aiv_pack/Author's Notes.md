
## Note: 
This set of aivs, along with the contents of this file, are several years old, specifically from 2010, which was when I uploaded the aiv pack here: http://stronghold.heavengames.com/downloads/showfile.php?fileid=6427


=============================
# United AIV - Author's Notes
=============================

_____ 

## Description of the AIVs
-----------------------

This is a series of 16 improved castles - 1 for every AI. 

I tried to create a good mix of improved economy and good defenses without ending up with a huge, boring square design. 
The 1-layer-castles are hard to enter for assassins (most of the wall is assassin-proof), 
include a moat (though e.g. Rat won't dig it), pitch ditch, dog cages. 

Moreover, they use boiling oil, killing pits, tower-mounted siege engines and fire ballistae to defend themselves. 
The AIV don't include trebuchets as these are more expensive (3rd engineer needed), need stones every now and then and 
actually hit their targets hardly ever. 

Castle shape and defense are quite the same for all the AI, that's why I call it "United AIV". 
Although some buildings are the same for all AI, there are big differences between the inside of the castles. 

Especially the weaker AI profit from this castle as they get both, improved defenses and improved economy. 
Snake e.g. manages defending against Saladin while collecting tons of gold. 
But also more complex AI profit a lot: Various traps and an efficient economy strengthen them a lot. 

The main weakness of the castle is that, once the enemy manages setting some of the gibbets/gallows outside on fire, 
the flames may spread around. Most of the AI will manage to stop the fire, 
but some of them have always set their water pots to sleep if there's no fire. These may get into a bit of troubles. 
The moat around the castle, however, can stop nasty slaves (if the castle is not on a hill/plateau), so any enemy needs 
at least a bunch of fire ballistae to set a "united AIV" aflame. 

The second "weakness" of the castle is the size: It's not huge, but also not very small: Medium-large. So it might not 
fit everywhere. 
_____ 

## How do I use the files? 
-----------------------

First of all, you should backup the normal AIVs. 
To do so, go to your /Firefly Studios/Stronghold Crusader/ folder, select the inside aiv-folder, copy and paste it 
and call the new folder "aiv_backup". 

Now, to use these new AIV, please copy the ".aiv"-files from the zip-archive and replace the existing aiv-files for 
the chosen character(s) in the aiv-folder. 

To get back your old AIV, please replace the files in your folder "aiv" with the content of the folder "backup-aiv". 

If you have any suggestions for improving these AIVs (or a particular one), just tell me by a comment or email, too. 
I'm glad for any helpful hints! 

If you want to play now without reading any more sentences that begin with "If you", just open SHC and have fun! 

If you read this, you decided to go on and shouldn't mind reading another sentence beginning with "If you". 
_____ 

## Description for each AI
-----------------------

- Have you ever wanted the Abbot to man his towers properly? 
With enough fletchers close to stockpile and weaponry he even has enough archers to guard his farms and 
to send dozens of them with every attack wave. To avoid his holy temples getting crumbled, he agreed on improving 
his defenses. 

- Have you always wondered, why Nizar doesn't make a proper castle instead of economically slowing-down, 
only on minimal height working system of moat? 
Enjoy the united defensive forces of tons of archers, various siege engines, pitch ditch and his "usual" killing pits 
taking out any approaching hostile unit, while his economy speeds up to provide enough gold for his expensive armies! 

- Caliph kinda' loves these BIG round towers and all the fortifications in front of them - no more ruined towers! 
Playing with his mangonel, selling xbows, laughing at Nizar's assassins falling into the new moat, inaugurating a 
big dungeon under his keep, frightening his enemies, feeding lazy peasants to a pack of wild dogs, that's life at 
its best for the Caliph! 

- Giving a castle with some high towers, golden-roofed buildings and a more profitable weapons production, a few 
fire ballistae and several "traps", surrounded by an original Crusade-proof moat to the mighty, clever Arabian 
Sultan Salah ad-Din may be wise if you got him as an ally as he will thank you by holding off any enemy. 
If you are his foe, you may better leave the desert - unless you want to end up as a dessert for hyenas. 

- Sultan Abdul would probably be jealous at Sultan Saladin, so I gave him a palace to live up his evil side and, 
finally, produce something. Instead of picking flowers from his gardens, he now can live in a castle shaped 
like one. 
And he shows that he actually CAN dig up a moat! Early on he recruits two slaves for this job, hoping that they 
are faster in digging than his castle walls are set up normally. He also shows that he CAN build walls quickly, 
if he wants! Unfortunately nobody told him yet that growing food would be a nice way to keep his people healthy... 

- Wazir has finally realized the advantage of selling weapons for profit. He is glad about his new favorite cruelty, 
hunting enemies with blood-thirsty dogs. Moreover, he could be convinced that only safe bakeries are good ones. 
He swapped the jags of his star-fortresses for fortificated towers and some room for a bunch of golden domes. 

- Emir just realized that his lookout-towers lay in ruins most of the time, when he got the plans for his new castle. 
He is very pleased that a better life is waiting for him now, without crumbled towers. And with a few more fletchers, 
manning his lookout-towers properly is also possible. 
Now, Emir can concentrate on the question, what to do against your irritating presence. 

- Have you ever wished a Snake that doesn't run out of gold and has a real closed castle? 
With enough poleturners and both, moat and walls, all around his castle, Snake will manage defending his castle 
with a great number of archers. 

- Have you ever wanted a Rat, who does manage to build an enclosed castle without any stairs leading outside? 
Here you are, complete with siege engines and a productive weapons production. Offensively still almost useless due 
to Rat's tactics, but now he is a by far worthier ally (he won't need a babysitter anymore)! 

- After visiting Snake, Nizar and being angry about his brave knights being stopped by the Sheriff's moat, King Phillip 
made the decision to have a moat all around his palace, too, and ordered a massive back wall to be 
constructed as well as some elegant towers, flanked by beautifully curved castle walls. Moreover, Phillip always 
wanted to increase his glorious power by setting up a mighty army and militarily beat all his opponents. 
The new weapons production enables him to do so - well, at least to try it... 

- Emperor Frederick, alias Barbarossa, learned swimming in his moat. After praying in the royal cathedral, he feels 
stronger and stronger and is now ready for conquering various states. 
He is supported by some proper round-towers, loaded with some siege engines and bought some fire ballistae to let 
his enemies burn. 

- Have you always wanted Marshal to recruit more troops? 
Watch him rise and unleash the powerful force of 20 knights attacking any hostile industry, while he keeps his own 
buildings safe behind thick walls (including a back wall)! His archers will keep his castle clean of enemies and 
improved defenses and a big weapons production make him a real tough enemy! 
Is this going to be a fair deal? Yes, it is! 

- Have you ever... ok, this is getting boring. You all know Richard the Lionheart, right? This great attacker with 
those smallish square castles without many men on the towers. Maybe he'd be glad for some castle plans including a 
real big weapons production, a new, fresh castle design, wall-protected towers and a bunch of flags and banners to 
put up above your keep? Let the lion roar! 

- I had to have a harsh dispute with the Wolf, in order to persuade him that a 1-layer-stronghold is economically 
better than huge, massive, labyrinthine fortresses. However, he recognized that round towers are harder to get down 
than square towers and that these thin, tall perimeter towers provide better protection than outer towers. 
Furthermore, he happily agreed on mounting two siege engines per tower at the front side. Swapping trebuchets 
for fire ballistae really caused him some head-aches at first, but he quickly began to love burning down any 
hostile building. Still he managed to smuggle 3 trebuchets onto his screenshot... (fixed) 

- You all know the nasty Sheriff. In his brand-new fortress, he might be even more harmful to any peaceful surrounding. 
He likes the moat, the gallows, the shiny and impressive cathedral as well as his central mace-makers and half a 
dozen of lookout-towers overlooking every direction (for planning his next mean invasion on your fortress). 

- And finally... Have you ever understood, why Pig trusts in a single row of low wall for protecting his back? Yes? 
Fine then, but still some improved defenses for him coming along with an increased weapons production may not be 
harmful as now Duke Truffe can concentrate on torturing prisoners in his dungeon, while others do the defending. 
Adding some siege engines to his castle really made him really happy, as he can now smash, burn and destroy even more 
hostile buildings and units. 
_____ 

## Bugs that may occur: 
-----------------------

- Some AI may not place units/replace fallen units on their towers correctly (Rat, Sultan); this has nothing to do 
  with this castle, but happens because of the "character" of the AI. 

- Some AI (who don't use oil in any way normally) may continue buying and selling 4 pitch unless there is oil in the 
  oil smelter; the economical harm of this useless trading may seem quite big, but whenever the AI are busy with 
  trading something else, they can't trade oil AND the extra-weapons-production will more than cover the costs. 

- Using these AIV in the Crusader trail missions may lead to strange results; nonetheless you can try them out 
  (Crusader won't crash). 


Cheers, 
Heroesflorian
